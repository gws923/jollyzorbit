// Copyright(c) 2017 Happy Rock Studios

#include "ZorbitsOrbits.h"

int main()
{
    ZorbitsOrbits::Ptr zo = new ZorbitsOrbits();
    zo->run();

    return EXIT_SUCCESS;
}
