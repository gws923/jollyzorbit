# Locate JollyLava game engine library
# This module defines:
# JOLLYLAVA_LIBRARIES the JollyLava library file.
# JOLLYLAVA_INCLUDE_DIR the directory containing JollyLava header files.
#
# This file was created by looking at Box2D's FindBox2D.cmake file.

set(JOLLYLAVA_INSTALL_DIR CACHE PATH
    "Location of JollyLava installation; to locate headers and libraries.")

# If already in cache:
if(JOLLYLAVA_INCLUDE_DIR)
    # Be silent.
    set(JOLLYLAVA_FIND_QUIETLY TRUE)
endif(JOLLYLAVA_INCLUDE_DIR)

# Finds directory containing the specified file.
find_path(JOLLYLAVA_INCLUDE_DIR JollyLava/Game.h
    PATH_SUFFIXES include
    PATHS ${JOLLYLAVA_INSTALL_DIR})

if(WINDOWS)
    set(JOLLYLAVA_LIB_PATH_SUFFIX bin)
else()
    set(JOLLYLAVA_LIB_PATH_SUFFIX lib)
endif()

find_library(JOLLYLAVA_LIBRARY NAMES JollyLava
    NO_CMAKE_PATH
    NO_CMAKE_ENVIRONMENT_PATH
    NO_SYSTEM_ENVIRONMENT_PATH
    NO_CMAKE_SYSTEM_PATH
    PATH_SUFFIXES ${JOLLYLAVA_LIB_PATH_SUFFIX}
    PATHS ${JOLLYLAVA_INSTALL_DIR})

mark_as_advanced(JOLLYLAVA_INCLUDE_DIR JOLLYLAVA_LIBRARY)

# I don't know what this is.
set(JOLLYLAVA_INCLUDE_DIRS "${JOLLYLAVA_INCLUDE_DIR}")
set(JOLLYLAVA_LIBRARIES "${JOLLYLAVA_LIBRARY}")

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(JollyLava DEFAULT_MSG JOLLYLAVA_INCLUDE_DIR JOLLYLAVA_LIBRARY)
